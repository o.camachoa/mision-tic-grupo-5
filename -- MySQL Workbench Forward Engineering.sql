-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema larockola
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `larockola` ;

-- -----------------------------------------------------
-- Schema larockola
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `larockola` DEFAULT CHARACTER SET utf8 ;
USE `larockola` ;

-- -----------------------------------------------------
-- Table `larockola`.`Persona`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `larockola`.`Persona` (
  `id_persona` INT NOT NULL AUTO_INCREMENT,
  `nro_identificacion` VARCHAR(12) NULL,
  `tipo_identificacion` CHAR(2) NULL,
  `nombres` VARCHAR(45) NULL,
  `apellidos` VARCHAR(45) NULL,
  PRIMARY KEY (`id_persona`),
  UNIQUE INDEX `nro_identifiacion_UNIQUE` (`nro_identificacion` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `larockola`.`Genero`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `larockola`.`Genero` (
  `id_genero` INT NOT NULL AUTO_INCREMENT,
  `desc_genero` VARCHAR(45) NULL,
  PRIMARY KEY (`id_genero`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `larockola`.`Idioma`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `larockola`.`Idioma` (
  `id_idioma` INT NOT NULL AUTO_INCREMENT,
  `desc_idioma` VARCHAR(45) NULL,
  PRIMARY KEY (`id_idioma`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `larockola`.`Artista`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `larockola`.`Artista` (
  `id_artista` INT NOT NULL AUTO_INCREMENT,
  `nombre_artista` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_artista`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `larockola`.`Cancion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `larockola`.`Cancion` (
  `id_cancion` INT NOT NULL AUTO_INCREMENT,
  `titulo` VARCHAR(45) NOT NULL,
  `id_idioma_fk` INT NOT NULL,
  `id_genero_fk` INT NOT NULL,
  `id_artista_fk` INT NOT NULL,
  PRIMARY KEY (`id_cancion`),
  INDEX `fk_cancion_idioma_idx` (`id_idioma_fk` ASC) VISIBLE,
  INDEX `fk_cancion_genero_idx` (`id_genero_fk` ASC) VISIBLE,
  INDEX `fk_cancion_artista_idx` (`id_artista_fk` ASC) VISIBLE,
  CONSTRAINT `fk_cancion_idioma`
    FOREIGN KEY (`id_idioma_fk`)
    REFERENCES `larockola`.`Idioma` (`id_idioma`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cancion_genero`
    FOREIGN KEY (`id_genero_fk`)
    REFERENCES `larockola`.`Genero` (`id_genero`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cancion_artista`
    FOREIGN KEY (`id_artista_fk`)
    REFERENCES `larockola`.`Artista` (`id_artista`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `larockola`.`Disquera`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `larockola`.`Disquera` (
  `id_disquera` INT NOT NULL AUTO_INCREMENT,
  `desc_disquera` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_disquera`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `larockola`.`Album`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `larockola`.`Album` (
  `id_album` INT NOT NULL AUTO_INCREMENT,
  `nombre_album` VARCHAR(45) NOT NULL,
  `fecha_album` DATE NULL,
  `formato_album` VARCHAR(4) NULL,
  `valor_album` DOUBLE NOT NULL,
  `id_disquera_fk` INT NULL,
  PRIMARY KEY (`id_album`),
  INDEX `fk_album_disquera_idx` (`id_disquera_fk` ASC) VISIBLE,
  CONSTRAINT `fk_album_disquera`
    FOREIGN KEY (`id_disquera_fk`)
    REFERENCES `larockola`.`Disquera` (`id_disquera`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `larockola`.`Album_Cancion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `larockola`.`Album_Cancion` (
  `id_album_fk` INT NOT NULL,
  `id_cancion_fk` INT NOT NULL,
  INDEX `fk_album_cancion_idx` (`id_cancion_fk` ASC) VISIBLE,
  INDEX `fk_album_cancion_idx1` (`id_album_fk` ASC) VISIBLE,
  CONSTRAINT `fk_cancion_album`
    FOREIGN KEY (`id_cancion_fk`)
    REFERENCES `larockola`.`Cancion` (`id_cancion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_album_cancion`
    FOREIGN KEY (`id_album_fk`)
    REFERENCES `larockola`.`Album` (`id_album`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `larockola`.`Factura`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `larockola`.`Factura` (
  `id_factura` INT NOT NULL AUTO_INCREMENT,
  `cod_factura` VARCHAR(10) NULL,
  `fecha_factura` TIMESTAMP NOT NULL,
  `id_cliente_fk` INT NOT NULL,
  PRIMARY KEY (`id_factura`),
  INDEX `fk_factura_persona_idx` (`id_cliente_fk` ASC) VISIBLE,
  CONSTRAINT `fk_factura_persona`
    FOREIGN KEY (`id_cliente_fk`)
    REFERENCES `larockola`.`Persona` (`id_persona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `larockola`.`Detalle_Factura`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `larockola`.`Detalle_Factura` (
  `id_factura_fk` INT NOT NULL,
  `id_album_fk` INT NOT NULL,
  `cantidad` INT NOT NULL,
  INDEX `fk_detalle_factura_album_idx` (`id_album_fk` ASC) VISIBLE,
  INDEX `fk_detalle_factura_factura_idx` (`id_factura_fk` ASC) VISIBLE,
  CONSTRAINT `fk_detalle_factura_factura`
    FOREIGN KEY (`id_factura_fk`)
    REFERENCES `larockola`.`Factura` (`id_factura`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalle_factura_album`
    FOREIGN KEY (`id_album_fk`)
    REFERENCES `larockola`.`Album` (`id_album`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
